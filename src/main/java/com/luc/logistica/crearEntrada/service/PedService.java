package com.luc.logistica.crearEntrada.service;

import com.luc.logistica.crearEntrada.model.Ped;
import java.util.List;

public interface PedService {
	public Ped findbyPed(Integer numero, String tipo);
	public Ped findbyId(String id);
	public List<Ped> findEnEspera();
	public List<Ped> findAll();
	public Ped savePed(Ped ped);
}
