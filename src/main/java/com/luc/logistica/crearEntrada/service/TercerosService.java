package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.Terceros;

public interface TercerosService {
	public Terceros findByTer(String codigoLuc);
	public Terceros findByEan(String ean);
	public Terceros findByNit(Double nit);
	public List<Terceros> findAll();
	public List<Terceros> findByTexto(String texto);
	public Terceros saveTer(Terceros ter);
}
