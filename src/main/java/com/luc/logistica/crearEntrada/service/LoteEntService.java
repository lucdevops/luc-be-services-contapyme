package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.LoteEnt;

public interface LoteEntService {
	public LoteEnt findByLote(String codigo, String lote);
	public List<LoteEnt> findByOrc(String numero);
	public List<LoteEnt> findAll();
	public LoteEnt saveEntLote(LoteEnt lt);
}

