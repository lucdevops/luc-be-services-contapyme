package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.ReferenciasSto;

public interface RefStoService {
	public ReferenciasSto findByReg(String codigo, Integer ano, Integer mes,Integer bodega);
	public List<ReferenciasSto> findAll();
	public ReferenciasSto saveRef(ReferenciasSto ref);
}
