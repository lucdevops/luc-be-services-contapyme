package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.Lote;

public interface LoteService {
	public List<Lote> findByOrc(String numero, String tipo);
	public Lote findByLote(String numero, String tipo, String codigo);
	public List<Lote> findAll();
	public Lote saveLote(Lote lt);
}

