package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.DetalleOC;

public interface DetalleOCService {
	public DetalleOC findById(Integer numero);
	public List<DetalleOC> findAll();
	public void saveDetOC(DetalleOC doc);
}

