package com.luc.logistica.crearEntrada.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.luc.logistica.crearEntrada.model.DetalleOCPed;
import com.luc.logistica.crearEntrada.model.EncabezadoOC;
import com.luc.logistica.crearEntrada.model.Impuesto;
import com.luc.logistica.crearEntrada.model.Lote;
import com.luc.logistica.crearEntrada.model.Marca;
import com.luc.logistica.crearEntrada.model.Ped;
import com.luc.logistica.crearEntrada.model.Precio;
import com.luc.logistica.crearEntrada.model.Terceros;
import com.luc.logistica.crearEntrada.model.Proveedor;
import com.luc.logistica.crearEntrada.model.Referencias;
import com.luc.logistica.crearEntrada.service.DetalleOCPedHisService;
import com.luc.logistica.crearEntrada.service.DetalleOCPedService;
import com.luc.logistica.crearEntrada.service.LoteService;
import com.luc.logistica.crearEntrada.service.MarcaService;
import com.luc.logistica.crearEntrada.service.OrdenCompraService;
import com.luc.logistica.crearEntrada.service.PedHisService;
import com.luc.logistica.crearEntrada.service.PedService;
import com.luc.logistica.crearEntrada.service.ProveedorService;
import com.luc.logistica.crearEntrada.service.ReferenciasService;
import com.luc.logistica.crearEntrada.service.TercerosService;
import com.luc.logistica.crearEntrada.model.CenData;
import com.luc.logistica.crearEntrada.service.LoteEntService;
import com.luc.logistica.crearEntrada.service.CenService;
import com.luc.logistica.crearEntrada.service.RefStoService;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RestController
@CrossOrigin(origins = { "*", "*" })
// @CrossOrigin(origins = { "http://192.168.1.250:8083", "*" })
@RequestMapping("/crear")

public class CrearController {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ProveedorService proveedorService;
	@Autowired
	private DetalleOCPedService docPedService;
	@Autowired
	private DetalleOCPedHisService docPedHisService;
	@Autowired
	private RefStoService refService;
	@Autowired
	private ReferenciasService refeService;
	@Autowired
	private LoteService loteService;
	@Autowired
	private LoteEntService loteOrcService;
	@Autowired
	private OrdenCompraService ocService;
	@Autowired
	private OrdenCompraService ordenCompraService;
	@Autowired
	private PedService pedService;
	@Autowired
	private PedHisService pedHisService;
	@Autowired
	private ReferenciasService referenciasService;
	@Autowired
	private TercerosService tercerosService;
	@Autowired
	private CenService cenService;
	@Autowired
	private MarcaService marcaService;
	@Autowired
	private LoteEntService loteEntService;
	/*
	 * @Autowired private PlanillaService planillaService;
	 * 
	 * @Autowired private PlanillaLinService planillaLinService;
	 */
	private StringBuffer buf;
	@Value("${contapyme.host}")
	private String ip;
	@Value("${contapyme.port}")
	private String puerto;
	private Random rnd = new Random();
	private String url = "";
	private String consulta = "";
	@Value("${contapyme.user}")
	private String usuario;
	@Value("${contapyme.password}")
	private String clave;
	@Value("${contapyme.app}")
	private String app;
	private String key = "";

	private void autenticaAgente() {

		try {
			consulta = "{\"email\":\"" + usuario + "\",\"password\":\"" + clave
					+ "\",\"idmaquina\":\"537.22_136301143299\"}";
			url = "http://" + ip + ":" + puerto + "/datasnap/rest/TBasicoGeneral/GetAuth/" + consulta + "/" + app + "/"
					+ rnd.nextInt(9999) + "/";

			URL url2 = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/xml");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			String output;
			String resultado = "";
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			while ((output = br.readLine()) != null) {
				resultado = resultado + output;
			}

			JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
			JSONArray result = (JSONArray) jsonObject.get("result");
			JSONObject arr = (JSONObject) result.get(0);
			JSONObject respuesta = (JSONObject) arr.get("respuesta");
			JSONObject datos = (JSONObject) respuesta.get("datos");
			key = (String) datos.get("keyagente");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean areaTrabajo(int a) {

		autenticaAgente();

		try {
			consulta = "{\"iemp\":\"" + a + "\"}";
			url = "http://" + ip + ":" + puerto + "/datasnap/rest/TBasicoGeneral/SetDatosTrabajo/" + consulta + "/"
					+ key + "/" + app + "/" + rnd.nextInt(9999) + "/";

			URL url2 = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/xml");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			String output;
			String resultado = "";
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			while ((output = br.readLine()) != null) {
				resultado = resultado + output;
			}

			JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
			JSONArray result = (JSONArray) jsonObject.get("result");
			JSONObject arr = (JSONObject) result.get(0);
			JSONObject respuesta = (JSONObject) arr.get("respuesta");
			JSONObject datos = (JSONObject) respuesta.get("datos");
			String cambio = (String) datos.get("modifico");
			if (cambio.equals("true")) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	// CREACION DE RECEPCION EN CONTAPYME, SE REQUIERE JSON DE PRODUCTOS, AREA EN LA
	// QUE SE CREARA, LA BODEGA QUE MANEJA EL PROVEEDOR Y EL NIT DEL PROVEEDOR
	@GetMapping("/crearRecepcion/{prod}/{area}/{bodega}/{proveedor}")
	public String[] crearRecepcion(@PathVariable String prod, @PathVariable String area, @PathVariable String bodega,
			@PathVariable String proveedor) {

		autenticaAgente();
		Date myDate = new Date();
		SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy");
		String fecha = mdyFormat.format(myDate);
		String creacion = "", operacion = "", oper = "";
		String[] resultados = new String[2];

		try {

			consulta = "{\"accion\":\"CREATE\",\"operaciones\":[{\"itdoper\":\"ORD6\"}],\"oprdata\":{\"encabezado\": {\"iemp\": \""
					+ area
					+ "\",\"tdetalle\": \"\",\"itdsop\": \"520\",\"inumsop\": \"0\",\"snumsop\": \"<AUTO>\",\"fsoport\": \""
					+ fecha
					+ "\",\"iclasifop\": \"1\",\"iprocess\": \"0\",\"banulada\": \"F\",\"svaloradic1\": \"RE\"},\"datosprincipales\": {\"init\": \""
					+ proveedor + "\",\"inittransportador\": \"\",\"sobserv\": \"\",\"iinventario\": \"" + bodega
					+ "\",\"ilistaprecios\": \"0\",\"blistaconiva\": \"F\",\"blistaconprecios\": \"T\",\"bregvrunit\": \"F\",\"bregvrtotal\": \"T\",\"ireferencia\":\"\",\"bcerrarref\": \"F\",\"breferenciabasadaencompras\": \"F\",\"qregseriesproductos\": \"1\"},\"listaproductos\": ["
					+ prod + "]}}";
			url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
					+ key + "/" + app + "/" + rnd.nextInt(9999) + "/";

			URL url2 = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/xml");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			String output;
			String resultado = "";
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			while ((output = br.readLine()) != null) {
				resultado = resultado + output;
			}

			JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
			JSONArray result = (JSONArray) jsonObject.get("result");
			JSONObject arr = (JSONObject) result.get(0);
			JSONObject respuesta = (JSONObject) arr.get("respuesta");
			JSONObject encabezado = (JSONObject) arr.get("encabezado");
			JSONObject datos = (JSONObject) respuesta.get("datos");
			oper = (String) datos.get("inumoper");
			operacion = (String) datos.get("snumsop");
			creacion = (String) encabezado.get("resultado");
			resultados[0] = creacion;
			resultados[1] = operacion;

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (creacion.equals("true")) {
			procesarRecepcion(oper, 2);
			return resultados;
		} else {
			return resultados;
		}

	}

	// CREACION DE FACTURA EN CONTAPYME, SE REQUIERE JSON DE PRODUCTOS, AREA EN LA
	// QUE SE CREARA, LA BODEGA QUE MANEJA EL PROVEEDOR, EL TIPO DE DCOUMENTO Y EL
	// NIT DEL CLIENTE
	@GetMapping("/crearFactura/{prod}/{area}/{tipoDoc}/{bodega}/{nit}//{valorNeto}/{lista}/{numFac}/{numPed}")
	public String[] crearFactura(@PathVariable String prod, @PathVariable String area, @PathVariable String bodega,
			@PathVariable String nit, @PathVariable String tipoDoc, @PathVariable String valorNeto,
			@PathVariable String lista, @PathVariable String numFac, @PathVariable Integer numPed)
			throws UnsupportedEncodingException, ParseException {

		autenticaAgente();
		Date myDate = new Date();

		SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy");
		Terceros ter = tercerosService.findByNit(Double.parseDouble(nit));
		String fecha = mdyFormat.format(myDate);
		Impuesto imp = calcularImpuestos(nit, bodega, lista, String.valueOf(ter.getVendedor()), valorNeto, tipoDoc,
				area, prod);
		String creacion = " ", numFV = " ", operCP = " ";
		// String impuestos = URLEncoder.encode(imp.getImpuesto(), "UTF-8");
		String[] resultados = new String[2];

		try {

			consulta = "{\"accion\":\"CREATE\",\"operaciones\":[{\"itdoper\":\"ING1\"}],\"oprdata\":{\"encabezado\":{\"iemp\":\""
					+ area + "\",\"inumoper\":\"\",\"itdsop\":\"" + tipoDoc + "\",\"fsoport\":\"" + fecha
					+ "\",\"iclasifop\":\"1\",\"imoneda\":\"10\",\"iprocess\":\"0\",\"banulada\":\"F\",\"inumsop\":\"\",\"snumsop\":\""
					+ numFac + "\",\"tdetalle\":\"FACTURA VITA AVE " + fecha + "\"},\"datosprincipales\":{\"init\":\""
					+ URLEncoder.encode(ter.getCodigoLuc(), "UTF-8") + "\",\"iinventario\":\"" + bodega
					+ "\",\"initvendedor\":\"" + BigDecimal.valueOf(ter.getVendedor())
					+ "\",\"busarotramoneda\":\"F\",\"sobserv\":\"\",\"imoneda\":\"\",\"mtasacambio\":\"1.0000\",\"bshowsupportinfo\":\"F\",\"bregvrunit\":\"F\",\"bshowcntfields\":\"F\",\"qporcdescuento\":\"0.0000\",\"bregvrtotal\":\"F\",\"ilistaprecios\":\""
					+ lista + "\"},\"listaproductos\":[" + prod + "],\"liquidimpuestos\":["
					+ URLEncoder.encode(imp.getImpuesto(), "UTF-8") + "],\"formacobro\":{\"mtotalreg\":\""
					+ imp.getTotal() + "\",\"mtotalpago\":\"" + imp.getTotal()
					+ "\",\"fcobrocxc\":[{\"id\":\"1\",\"init\":\"" + nit
					+ "\",\"icuenta\":\"130505\",\"qdiascxc\":\"\",\"icc\":\"\",\"mvalor\":\"" + imp.getTotal()
					+ "\"}]},\"listaanexos\":[],\"qoprsok\":\"0\"}}";
			url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
					+ key + "/" + app + "/" + rnd.nextInt(9999) + "/";

			URL url2 = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/xml");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			String output;
			String resultado = "";
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			while ((output = br.readLine()) != null) {
				resultado = resultado + output;
			}

			JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
			JSONArray result = (JSONArray) jsonObject.get("result");
			JSONObject arr = (JSONObject) result.get(0);
			JSONObject respuesta = (JSONObject) arr.get("respuesta");
			JSONObject encabezado = (JSONObject) arr.get("encabezado");
			JSONObject datos = (JSONObject) respuesta.get("datos");
			operCP = (String) datos.get("inumoper");
			numFV = (String) datos.get("snumsop");
			creacion = (String) encabezado.get("resultado");
			resultados[0] = creacion;
			resultados[1] = numFV;

			Ped p = pedService.findbyPed(numPed, "REP");
			p.setNotas(numFac);
			p.setEstado("FI");
			pedService.savePed(p);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return resultados;
	}

	public Impuesto calcularImpuestos(String nit, String bodega, String lista, String vendedor, String valorNeto,
			String tipoDoc, String area, String productos) {

		autenticaAgente();
		Impuesto retorno = new Impuesto();
		String impuestos = "";
		Date myDate = new Date();
		SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy");
		String fecha = mdyFormat.format(myDate);
		double neto = Double.parseDouble(valorNeto);

		try {
			consulta = "{\"accion\":\"CALCULARIMPUESTOS\",\"operaciones\":[{\"inumoper\":\"\",\"itdoper\":\"ING1\"}],\"oprdata\":{\"datosprincipales\":{\"init\":\""
					+ nit + "\",\"iinventario\":\"" + bodega + "\",\"initvendedor\":\"" + vendedor
					+ "\",\"initvendedor2\":\"\",\"busarotramoneda\":\"F\",\"sobserv\":\"\",\"imoneda\":\"10\",\"qporcdescuento\":\"0.0000\",\"ilistaprecios\":\""
					+ lista
					+ "\",\"blistaconiva\": \"F\",\"isucursalcliente\":\"-1\",\"bfacturaexportacion\":\"F\"},\"encabezado\":{\"iemp\":\""
					+ area + "\",\"inumoper\":\"\",\"tdetalle\":\"\",\"itdsop\":\"" + tipoDoc
					+ "\",\"inumsop\":\"0\",\"snumsop\":\"\",\"fsoport\":\"" + fecha
					+ "\",\"iclasifop\":\"0\",\"imoneda\":\"10\"},\"listaproductos\":[" + productos + "]}}";
			url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
					+ key + "/" + app + "/" + rnd.nextInt(9999) + "/";

			URL url2 = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/xml");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			String output;
			String resultado = "";
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			while ((output = br.readLine()) != null) {
				resultado = resultado + output;
			}

			JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
			JSONArray result = (JSONArray) jsonObject.get("result");
			JSONObject arr = (JSONObject) result.get(0);
			JSONObject respuesta = (JSONObject) arr.get("respuesta");
			JSONObject datos = (JSONObject) respuesta.get("datos");
			JSONObject resultado1 = (JSONObject) datos.get("resultado");
			JSONArray liquida = (JSONArray) resultado1.get("liquidimpuestos");
			Terceros ter = tercerosService.findByNit(Double.parseDouble(nit));

			for (int i = 0; i < liquida.size(); i++) {
				JSONObject arreglo = (JSONObject) liquida.get(i);
				String signo = (String) arreglo.get("isigno");
				double valor = Double.parseDouble((String) arreglo.get("mvalor"));
				retorno.setIva(valor);
				arreglo.put("init", String.valueOf(BigDecimal.valueOf(ter.getNitReal())));

				if (signo.equals("+")) {
					impuestos += liquida.get(i).toString();
					neto += valor;
				}
			}

			DecimalFormat df = new DecimalFormat("0");
			String valor = df.format(neto);
			neto = Double.parseDouble(valor);
			retorno.setImpuesto(impuestos);
			retorno.setTotal(neto);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}

	private boolean procesarRecepcion(String operacion, Integer area) {

		areaTrabajo(area);

		try {
			consulta = "{\"accion\":\"PROCESS\",\"operaciones\":[{\"inumoper\":\"" + operacion
					+ "\",\"itdoper\":\"ORD6\"}]}";
			url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
					+ key + "/" + app + "/" + rnd.nextInt(9999) + "/";

			URL url2 = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/xml");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			String output;
			String resultado = "";
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			while ((output = br.readLine()) != null) {
				resultado = resultado + output;
			}

			JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
			JSONArray result = (JSONArray) jsonObject.get("result");
			JSONObject arr = (JSONObject) result.get(0);
			JSONObject respuesta = (JSONObject) arr.get("respuesta");
			JSONObject datos = (JSONObject) respuesta.get("datos");
			String borro = (String) datos.get("resultado");

			if (borro.equals("T")) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@GetMapping("/crearPedidoCP/{dia}")
	public void cargaCliDia(@PathVariable String dia) {
		Calendar now = Calendar.getInstance();
		String[] strDays = new String[] { "DOMINGO", "LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO" };

	}

	@GetMapping("/crearOrc/{det}/{numero}/{tipo}")
	public String crearOrc(@PathVariable String det, @PathVariable Integer numero, @PathVariable String tipo) {

		Ped p = pedService.findbyPed(numero, tipo);
		Terceros prov = tercerosService.findByTer(p.getProveedor());
		Proveedor prov2 = proveedorService.findById(prov.getVd());
		String prod = jsonProductos(det, prov2.getBodega());
		String r = "";

		autenticaAgente();
		Date myDate = new Date();
		DecimalFormat df = new DecimalFormat("0.00");
		SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy");
		String fecha = mdyFormat.format(myDate);
		String detalle = "ORDEN DE COMPRA DE " + prov.getNombres() + " " + fecha;

		int area = 1;
		int tdoc = 510;
		if (!tipo.equals("ORC")) {
			area = 2;
			tdoc = 511;
		}

		try {

			consulta = "{\"accion\":\"CREATE\",\"operaciones\":[{\"itdoper\":\"ORD5\"}],\"oprdata\":{\"encabezado\": {\"iemp\": \""
					+ area + "\",\"tdetalle\": \"" + detalle + "\",\"itdsop\": \"" + tdoc
					+ "\",\"inumsop\": \"0\",\"snumsop\": \"<AUTO>\",\"fsoport\": \"" + fecha
					+ "\",\"iclasifop\": \"1\",\"iprocess\": \"0\",\"banulada\": \"F\",\"svaloradic1\": \"RE\"},\"datosprincipales\": {\"init\": \""
					+ p.getTercero().getCodigoLuc()
					+ "\", \"inittransportador\": \"\",\"sobserv\": \"\",\"iinventario\": \"" + prov2.getBodega()
					+ "\",\"ilistaprecios\": \"0\",\"blistaconiva\": \"F\",\"blistaconprecios\": \"T\",\"bregvrunit\": \"F\",\"bregvrtotal\": \"T\",\"ireferencia\":\"\",\"bcerrarref\": \"F\",\"breferenciabasadaencompras\": \"F\",\"qregseriesproductos\": \"1\"},\"listaproductos\": ["
					+ prod + "]}}";
			url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
					+ key + "/" + app + "/" + rnd.nextInt(9999) + "/";

			URL url2 = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/xml");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			String output;
			String resultado = "";
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			while ((output = br.readLine()) != null) {
				resultado = resultado + output;
			}

			JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
			JSONArray result = (JSONArray) jsonObject.get("result");
			JSONObject arr = (JSONObject) result.get(0);
			JSONObject respuesta = (JSONObject) arr.get("respuesta");
			JSONObject encabezado = (JSONObject) arr.get("encabezado");
			JSONObject datos = (JSONObject) respuesta.get("datos");
			System.out.println(encabezado.get("resultado"));
			r = (String) encabezado.get("resultado");

			p.setEstado("PE");
			pedService.savePed(p);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return r;

	}

	/* JSON PARA LOS PRODUCTOS DE CADA FACTURA */
	public String jsonProductos(String detalle, String bodega) {

		String info[] = detalle.split("}");
		String results[] = new String[2];
		String prod = "", separador = "";

		for (int i = 0; i < info.length; i++) {
			info[i] = info[i].substring(1);
			String dates[] = info[i].split("-");
			Referencias ref = refeService.findByRefe(dates[0]);
			double neto = 0;

			results[0] = ref.getDescripcion();
			if (ref.getMarca().getId().equals("01") || ref.getMarca().getId().equals("02")
					|| ref.getMarca().getId().equals("09") || ref.getMarca().getId().equals("10")) {
				if (ref.getCosto() != null) {
					neto = ref.getCosto() * Double.parseDouble(dates[1]);
				}

			}

			if (prod.equals("")) {
				separador = "";
			} else {
				separador = ",";
			}

			prod = prod + separador + "{\"icc\":\"1\",\"iinventario\":" + bodega + ",\"irecurso\":\"" + ref.getCodigo()
					+ "\",\"itiporec\":\"\",\"qproducto\":\"" + dates[1] + "\",\"qrecurso\":\"" + dates[1]
					+ "\",\"mprecio\":\"" + ref.getCosto() + "\",\"qporcdescuento\":\"\",\"qporciva\":\"" + ref.getIva()
					+ "\",\"mvrtotal\":\"" + neto + "\"}";

		}

		return prod;

	}

	public String jsonProductosOrc(List<DetalleOCPed> detOrc, Ped orc) {

		String prod = "", separador = "";
		Terceros vd = tercerosService.findByTer(orc.getProveedor());
		Proveedor vd2 = proveedorService.findById(vd.getVd());

		for (int i = 0; i < detOrc.size(); i++) {

			if (prod.equals("")) {
				separador = "";
			} else {
				separador = ",";
			}

			if (detOrc.get(i).getCantidadRec().doubleValue() > 0.0D) {
				prod = prod + separador + "{\"iinventario\":\"" + vd2.getBodega() + "\",\"irecurso\":\""
						+ detOrc.get(i).getRefe().getCodigo() + "\",\"itiporec\":\"\",\"qrecurso\":\""
						+ detOrc.get(i).getCantidadRec()
						+ "\",\"mprecio\":\"0.0000\",\"qporcdescuento\":\"0.0000\",\"qporciva\":\"0.0000\",\"mvrtotal\":\"0.0000\",\"icc\":\"\",\"sobserv\":\"\",\"dato1\":\"\",\"dato2\":\"\",\"dato3\":\"\",\"dato4\":\"\",\"dato5\":\"\",\"dato6\":\"\",\"valor1\":\"0.0000\",\"valor2\":\"0.0000\",\"valor3\":\"0.0000\",\"valor4\":\"0.0000\"}";

			}
		}

		return prod;

	}

	public String jsonLotes(List<Lote> loteOrc, Ped orc) throws UnsupportedEncodingException {

		String lotes = "", separador = "";
		Terceros vd = tercerosService.findByTer(orc.getProveedor());
		SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy");

		for (int i = 0; i < loteOrc.size(); i++) {

			String fecha = URLEncoder.encode(mdyFormat.format(loteOrc.get(i).getVencimiento()), "UTF-8");

			if (lotes.equals("")) {
				separador = "";
			} else {
				separador = ",";
			}

			lotes = lotes + separador + "{\"iinventario\":\"1\",\"irecurso\":\"" + loteOrc.get(i).getCodigo()
					+ "\",\"iserie\":\"" + loteOrc.get(i).getLote()
					+ "\",\"irecursopadre\":\"\",\"iseriepadre\":\"\",\"qrecurso\":\"" + loteOrc.get(i).getCantidad()
					+ "\",\"fecha1\":\"" + fecha + "\",\"fecha2\":\"" + fecha
					+ "\",\"dato1\":\"\",\"dato2\":\"\",\"dato3\":\"\",\"dato4\":\"\",\"valor1\":\"0.0000\",\"valor2\":\"0.0000\",\"notas\":\"\",\"inumsoporigen\":\"\"}";

		}

		return lotes;
	}

	@GetMapping("/capturarPedido/{marca}/{ordenC}/{cliente}/{prods}")
	public void capturarPedido(@PathVariable String marca, @PathVariable String ordenC, @PathVariable Double cliente,
			@PathVariable String prods) {

		try {

			autenticaAgente();
			Proveedor completo;
			Marca m = marcaService.findById(marca);

			if (m.getNombre().equals("EL REY") || m.getNombre().equals("TRIGUISAR") || m.getNombre().equals("KATORI")
					|| m.getNombre().equals("PHILIPS")) {
				completo = proveedorService.findByName("LINEA UNO");
			} else {
				completo = proveedorService.findByName(m.getNombre());
			}

			/* Proveedor completo = proveedorService.findById(marca); */
			/* Proveedor completo = proveedorService.findByName(m.nombre); */

			String separador = "", productos = "";
			Terceros clienteCarga = new Terceros();
			clienteCarga = tercerosService.findByNit(cliente);
			int errores = 0;
			double descuento, total, precio;
			buf = new StringBuffer();

			String separadorProducto[] = prods.split("-");
			for (int i = 0; i < separadorProducto.length; i++) {
				if (i > 0) {
					separadorProducto[i] = separadorProducto[i].substring(1);
				}
				String datos[] = separadorProducto[i].split(",");

				Referencias articuloCarga = referenciasService.findByRefe(datos[0]);
				if (!clienteCarga.getListaPrecio().equals("") && clienteCarga.getVendedor() != 0.0) {
					if (articuloCarga != null) {
						Precio prec = obtenerPrecio(completo.getBodega(), clienteCarga.getListaPrecio(),
								clienteCarga.getCodigoLuc(), articuloCarga.getCodigo());
						if (prec.getPrecio() > 0) {

							if (productos.equals("")) {
								separador = "";
							} else {
								separador = ",";
							}

							precio = (Double.parseDouble(datos[1])) * prec.getPrecio();
							descuento = prec.getDescuento() / 100;
							total = precio - (precio * descuento);
							DecimalFormat df = new DecimalFormat("0");
							String vNeto = df.format(total);

							productos = productos + separador + "{\"iinventario\":" + completo.getBodega()
									+ ",\"irecurso\":\"" + articuloCarga.getCodigo()
									+ "\",\"itiporec\":\"\",\"qrecurso\":" + datos[1] + ",\"mprecio\":\""
									+ prec.getPrecio() + "\",\"qporcdescuento\":\"" + prec.getDescuento()
									+ "\",\"qporciva\":\"" + prec.getImpuesto() + "\",\"mvrtotal\":\"" + vNeto
									+ "\",\"sobserv\":\"\",\"dato1\":\"\",\"dato2\":\"\",\"dato3\":\"\",\"dato4\":\"\",\"dato5\":\"\",\"dato6\":\"\",\"valor1\":\"\",\"valor2\":\"\",\"valor3\":\"\",\"valor4\":\"\"}";
						} else {
							buf.append("<p>-Articulo " + articuloCarga.getCodigo() + " sin precio para cliente "
									+ clienteCarga.getCodigoLuc() + " y lista " + clienteCarga.getListaPrecio()
									+ "</p>");
							errores++;
						}
					} else {
						buf.append("<p>-No se encontro el Articulo " + datos[0] + "</p>");
						errores++;
					}
				} else {
					buf.append("<p>-No se encontro el vendedor para el cliente o no tiene lista de precio "
							+ clienteCarga.getCodigoLuc() + "</p>");
					errores++;
				}

			}

			errores = 0;
			buf.append("<p>Procesando orden: " + ordenC + "</p>");

			/*
			 * if (!clienteCarga.getListaPrecio().equals("") && clienteCarga.getVendedor()
			 * != 0.0) { if (articuloCarga != null) { Precio prec =
			 * obtenerPrecio(completo.getBodega(), clienteCarga.getListaPrecio(),
			 * clienteCarga.getCodigoLuc(), articuloCarga.getCodigo()); if (prec.getPrecio()
			 * > 0) { double netoCP = prec.getPrecio() * (1 - (prec.getDescuento() / 100));
			 * 
			 * if (!completo.getNombre().equals("FRUTALIA")) { if ((neto - netoCP) > 100.00
			 * || (neto - netoCP) < -100.00) { buf.append("<p class='error'>Articulo " +
			 * articuloCarga.getCodigo() + " tiene diferencia en el descuento % </p>");
			 * errores++; }
			 * 
			 * if (bruto != prec.getPrecio()) { buf.append("<p class='error'>Articulo " +
			 * articuloCarga.getCodigo() + " tiene diferencia en el precio $, Precio: " +
			 * bruto + "</p>"); errores++; } } else { if (bruto != prec.getPrecio()) {
			 * buf.append("<p class='error'>Articulo " + articuloCarga.getCodigo() +
			 * " tiene diferencia en el precio $, Precio: " + bruto + "</p>"); errores++; }
			 * }
			 * 
			 * // descuento = (1 - (neto/bruto)) * 100; // descuento = ((bruto -neto)/bruto)
			 * * // 100;
			 * 
			 * if (productos.equals("")) { separador = ""; } else { separador = ","; }
			 * 
			 * String p = "";
			 * 
			 * if (!completo.getId().equals(p)) {
			 * buf.append("<p class='error'>Proveedor diferente al de las referencias</p>");
			 * errores++; }
			 * 
			 * precio = (Double.parseDouble(qty)) * prec.getPrecio(); descuento =
			 * prec.getDescuento() / 100; total = precio - (precio * descuento);
			 * DecimalFormat df = new DecimalFormat("0"); String vNeto = df.format(total);
			 * 
			 * productos = productos + separador + "{\"iinventario\":" +
			 * completo.getBodega() + ",\"irecurso\":\"" + articuloCarga.getCodigo() +
			 * "\",\"itiporec\":\"\",\"qrecurso\":" + qty + ",\"mprecio\":\"" +
			 * prec.getPrecio() + "\",\"qporcdescuento\":\"" + prec.getDescuento() +
			 * "\",\"qporciva\":\"" + prec.getImpuesto() + "\",\"mvrtotal\":\"" + vNeto +
			 * "\",\"sobserv\":\"\",\"dato1\":\"\",\"dato2\":\"\",\"dato3\":\"\",\"dato4\":\"\",\"dato5\":\"\",\"dato6\":\"\",\"valor1\":\"\",\"valor2\":\"\",\"valor3\":\"\",\"valor4\":\"\"}";
			 * } else { buf.append("<p>-Articulo " + articuloCarga.getCodigo() +
			 * " sin precio para cliente " + clienteCarga.getCodigoLuc() + " y lista " +
			 * clienteCarga.getListaPrecio() + "</p>"); errores++; } } else {
			 * buf.append("<p>-No se encontro el Articulo " + data[3] + "</p>"); errores++;
			 * } } else { buf.
			 * append("<p>-No se encontro el vendedor para el cliente o no tiene lista de precio "
			 * + clienteCarga.getCodigoLuc() + "</p>"); errores++; }
			 */

			if (!ordenC.equals("") && errores == 0) {

				if (errores == 0) {
					crearCotizacion(completo, ordenC, clienteCarga, productos);
				}
			}

			buf.append("</p>" + "Proceso finalizado" + "</p>");

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private void crearCotizacion(Proveedor prov, String orden, Terceros tercero, String productos)
			throws UnsupportedEncodingException {
		CenData cen = cenService.findById(orden);
		if (cen == null) {

			Date myDate = new Date();
			SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy");
			String fecha = mdyFormat.format(myDate);
			String vendedor = String.format("%.0f", tercero.getVendedor());

			try {
				consulta = "{\"accion\":\"CREATE\", \"operaciones\":[{\"itdoper\":\"ORD4\"}],\"oprdata\":{ \"datosprincipales\":{\"init\":\""
						+ URLEncoder.encode(tercero.getCodigoLuc(), "UTF-8") + "\", \"initvendedor\":\"" + vendedor
						+ "\", \"finicio\":\"" + fecha + "\", \"qdias\":\"5\", \"ilistaprecios\":\""
						+ tercero.getListaPrecio()
						+ "\", \"sobservenc\":\"\", \"bregvrunit\":\"F\", \"qporcdescuento\":\"\", \"bregvrtotal\":\"F\", \"frmenvio\":\"\", \"frmpago\":\"1\", \"condicion1\":\"\", \"blistaconiva\":\"F\",\"busarotramoneda\":\"F\", \"imonedaimpresion\":\"\", \"mtasacambio\":\"\", \"ireferencia\":\"\", \"bcerrarref\":\"F\", \"itdprintobs\":\"-1\",\"icontactocliente\":\"\"},\"encabezado\":{ \"iemp\":\""
						+ "1" + "\", \"itdsop\":\"410\", \"fsoport\":\"" + fecha
						+ "\",\"iclasifop\":\"0\", \"imoneda\":\"10\", \"iprocess\":\"0\", \"banulada\":\"F\", \"inumsop\":\"0\", \"snumsop\":\"<AUTO>\",\"tdetalle\":\""
						+ orden + "\", \"svaloradic1\":\"RE\", \"svaloradic2\":\"" + orden + "\", \"svaloradic3\":\""
						+ prov.getNombre()
						+ "\"},\"formacobro\":{},\"liquidacion\":{\"parcial\":\"\",\"descuento\":\"\",\"iva\":\"\",\"total\":\"\"},\"listaproductos\":["
						+ productos + "],\"listaanexos\":[],\"qoprsok\":\"0\"}}";
				url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta
						+ "/" + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

				URL url2 = new URL(url);
				HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/xml");

				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
				}
				String output;
				String resultado = "";
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

				while ((output = br.readLine()) != null) {
					resultado = resultado + output;
				}

				JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
				JSONArray result = (JSONArray) jsonObject.get("result");
				JSONObject arr = (JSONObject) result.get(0);
				JSONObject respuesta = (JSONObject) arr.get("respuesta");
				JSONObject datos = (JSONObject) respuesta.get("datos");
				String numero = String.valueOf(datos.get("inumoper"));
				String cotizacion = String.valueOf(datos.get("snumsop"));

				if (!numero.toString().equals("")) {
					CenData nuevo = new CenData();
					nuevo.setId(orden);
					nuevo.setProveedor(prov.getId());
					nuevo.setFecha(myDate);
					cenService.saveCen(nuevo);
					procesarCoti(numero);
					buf.append("<p>Crea cotizacion: " + cotizacion + "</p>");
					buf.append("<p>=======OK=======" + "</p>");
				} else {
					buf.append("<p>=======ERROR INESPERADO=======" + "</p>");
				}
			} catch (IOException e) {
				buf.append("<p>=======ERROR INESPERADO=======" + e.toString() + "</p>");
			}
		} else {
			buf.append("<p>=======Orden creada anteriormente=======" + "</p>");
		}
	}

	private boolean procesarCoti(String operacion) {

		areaTrabajo(1);

		try {
			consulta = "{\"accion\":\"PROCESS\", \"operaciones\":[{\"inumoper\":\"" + operacion
					+ "\", \"itdoper\":\"ORD4\"}]}";
			url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
					+ key + "/" + app + "/" + rnd.nextInt(9999) + "/";

			URL url2 = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/xml");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			String output;
			String resultado = "";
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			while ((output = br.readLine()) != null) {
				resultado = resultado + output;
			}

			log.debug("Hace consulta a vendedor " + url);
			JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
			JSONArray result = (JSONArray) jsonObject.get("result");
			JSONObject arr = (JSONObject) result.get(0);
			JSONObject respuesta = (JSONObject) arr.get("respuesta");
			JSONObject datos = (JSONObject) respuesta.get("datos");
			String borro = String.valueOf(datos.get("resultado"));

			if (borro.equals("T")) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private Precio obtenerPrecio(String inventario, String listaprecios, String nit, String articulo)
			throws UnsupportedEncodingException {
		Precio retorno = new Precio();

		Date myDate = new Date();
		SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy");
		String fecha = mdyFormat.format(myDate);

		try {

			consulta = "{\"iinventario\":\"" + inventario + "\",\"ilistaprecios\":\"" + listaprecios + "\",\"init\":\""
					+ URLEncoder.encode(nit, "UTF-8") + "\",\"iproducto\":\"" + articulo + "\",\"fsoport\":\"" + fecha
					+ "\"}";
			url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/GetValoresVentaProducto/" + consulta
					+ "/" + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

			URL url2 = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/xml");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			String output;
			String resultado = "";
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			while ((output = br.readLine()) != null) {
				resultado = resultado + output;
			}

			JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
			JSONArray result = (JSONArray) jsonObject.get("result");
			JSONObject arr = (JSONObject) result.get(0);
			JSONObject respuesta = (JSONObject) arr.get("respuesta");
			JSONObject datos = (JSONObject) respuesta.get("datos");
			double precio = Double.parseDouble((String.valueOf(datos.get("mprecio")).replace(",", ".")));
			double descuento = Double.parseDouble((String.valueOf(datos.get("qporcdescuento")).replace(",", ".")));
			double iva = Double.parseDouble((String.valueOf(datos.get("qporciva")).replace(",", ".")));
			retorno.setPrecio(precio);
			retorno.setImpuesto(iva);
			retorno.setDescuento(descuento);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}
}
