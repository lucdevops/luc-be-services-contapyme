package com.luc.logistica.crearEntrada.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.luc.logistica.crearEntrada.model.DetalleOCPed;
import com.luc.logistica.crearEntrada.model.Terceros;
import com.luc.logistica.crearEntrada.service.DetalleOCPedService;
import com.luc.logistica.crearEntrada.service.MarcaService;
import com.luc.logistica.crearEntrada.service.TercerosService;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@CrossOrigin(origins = { "*", "*" })
// @CrossOrigin(origins = { "http://192.168.1.250:8083", "*" })
@RequestMapping("/datos")

public class DatosController {

	@Autowired
	private TercerosService tercerosService;
	@Autowired
	private DetalleOCPedService detPedService;

	@Value("${contapyme.host}")
	private String ip;
	@Value("${contapyme.port}")
	private String puerto;
	private Random rnd = new Random();
	private String url = "";
	private String consulta = "";
	@Value("${contapyme.user}")
	private String usuario;
	@Value("${contapyme.password}")
	private String clave;
	@Value("${contapyme.app}")
	private String app;
	private String key = "";

	private void autenticaAgente() {

		try {
			consulta = "{\"email\":\"" + usuario + "\",\"password\":\"" + clave
					+ "\",\"idmaquina\":\"537.22_136301143299\"}";
			url = "http://" + ip + ":" + puerto + "/datasnap/rest/TBasicoGeneral/GetAuth/" + consulta + "/" + app + "/"
					+ rnd.nextInt(9999) + "/";

			URL url2 = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/xml");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			String output;
			String resultado = "";
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			while ((output = br.readLine()) != null) {
				resultado = resultado + output;
			}

			JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
			JSONArray result = (JSONArray) jsonObject.get("result");
			JSONObject arr = (JSONObject) result.get(0);
			JSONObject respuesta = (JSONObject) arr.get("respuesta");
			JSONObject datos = (JSONObject) respuesta.get("datos");
			key = (String) datos.get("keyagente");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@GetMapping("/referencia/{lista}/{nit}/{inventario}/{codigo}")
	public String[] jsonProds(@PathVariable String codigo, @PathVariable String lista, @PathVariable String nit,
			@PathVariable String inventario) throws UnsupportedEncodingException {

		autenticaAgente();
		Date myDate = new Date();
		DecimalFormat df = new DecimalFormat("0.00");
		SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy");
		String fecha = mdyFormat.format(myDate);
		String prod = "", separador;
		double precio = 0, descuento = 0, iva = 0, total = 0, valorNeto = 0;
		String info[] = codigo.split("}");
		String results[] = new String[2];

		for (int i = 0; i < info.length; i++) {
			info[i] = info[i].substring(1);
			String dates[] = info[i].split("-");

			try {

				consulta = "{\"iinventario\":\"" + inventario + "\",\"ilistaprecios\":\"" + lista + "\",\"init\":\""
						+ URLEncoder.encode(nit, "UTF-8") + "\",\"iproducto\":\"" + dates[0] + "\",\"fsoport\":\""
						+ fecha + "\"}";
				url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/GetValoresVentaProducto/"
						+ consulta + "/" + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

				URL url2 = new URL(url);
				HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/xml");

				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
				}
				String output;
				String resultado = "";
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

				while ((output = br.readLine()) != null) {
					resultado = resultado + output;
				}

				JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
				JSONArray result = (JSONArray) jsonObject.get("result");
				JSONObject arr = (JSONObject) result.get(0);
				JSONObject respuesta = (JSONObject) arr.get("respuesta");
				JSONObject datos = (JSONObject) respuesta.get("datos");
				precio = Double.parseDouble(((String) datos.get("mprecio")).replace(",", "."));
				descuento = Double.parseDouble(((String) datos.get("qporcdescuento")).replace(",", "."));
				iva = Double.parseDouble(((String) datos.get("qporciva")).replace(",", "."));
				total = Double.parseDouble(dates[1]) * (precio - (precio * (descuento / 100)));
				total = Double.parseDouble(df.format(total));
				valorNeto += total;

			} catch (Exception e) {
				e.printStackTrace();
			}

			if (prod.equals("")) {
				separador = "";
			} else {
				separador = ",";
			}

			prod += separador + "{\"icc\":\"1\",\"iinventario\":" + inventario + ",\"irecurso\":\"" + dates[0]
					+ "\",\"itiporec\":\"\",\"qproducto\":\"" + dates[1] + "\",\"qrecurso\":\"" + dates[1]
					+ "\",\"mprecio\":\"" + precio + "\",\"qporcdescuento\":\"" + descuento + "\",\"qporciva\":\"" + iva
					+ "\",\"mvrtotal\":\"" + total + "\"}";

		}

		results[0] = prod;
		results[1] = String.valueOf(valorNeto);

		return results;

	}

	

}
