package com.luc.logistica.crearEntrada.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearEntrada.model.DetalleOCPedHis;

@Repository("detalleOCPedHisRepository")
public interface DetalleOCPedHisRepository extends JpaRepository <DetalleOCPedHis, Integer>{
	
	@Query("select doch from DetalleOCPedHis doch where doch.id = ?1")
	DetalleOCPedHis findById(Integer id);
}
