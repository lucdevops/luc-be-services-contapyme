package com.luc.logistica.crearEntrada.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.luc.logistica.crearEntrada.model.Lote;

@Repository("loteRepository")
public interface LoteRepository extends JpaRepository<Lote, Integer> {

	@Query("select l from Lote l where l.numero = ?1 AND l.tipo = ?2")
	List<Lote> findByOrc(String numero, String tipo);

	@Query("select l from Lote l where l.numero = ?1 AND l.tipo = ?2 AND l.codigo = ?3")
	Lote findByLote(String numero, String tipo, String codigo);
}